# ELODY

[Go back](README.md)
## Summary

1. [Data Schema](#data-schema)
   1. [Excel Tabs List](#excel-tabs-list)
   2. [Rules](#rules)
   3. [Typical Property Checking](#typical-property-checking)
   4. [Reference Rule](#reference-rule)
   5. [Mapping Rule](#mapping-rule)
   6. [Classification Rule](#classification-rule)
2. [Excel Tabs](#excel-tabs)
   1. [File Data Tab](#file-data-tab)
      1. [Filename](#filename)
      2. [Levels](#levels)
      3. [Project Information](#project-information)
   2. [IFC Data Tab](#ifc-data-tab)
      1. [IfcProject](#ifcproject)
      2. [IfcSite](#ifcsite)
      3. [IfcBuilding](#ifcbuilding)
   3. [Generic Properties Tab](#generic-properties-tab)
   4. [Classification](#classification)
   5. [Ignore Objects](#ignore-objects)
   6. [Geo Markers](#geo-markers)
2. [Examples](#examples)


## Data Schema

This section will describe the way to define data schema in the Excel configuration file.

### Excel Tabs List

The Excel tabs in the configuration file have different scopes according to their name.

| Tab Name       | Description                                                                                   |
| -------------- | --------------------------------------------------------------------------------------------- |
| FILE DATA      | Data schema for file data verification (filename and/or levels)                               |
| GEN\*          | All Tabs starting with "GEN" will be general rules applied to all the objects                 |
| IFC            | Properties and rules by IFC class                                                             |
| IGNORE         | Object to ignore for the model check (objects are listed as ignored in the report)            |
| EXCLUDE        | Object to exclude for the model check (objects are not listed in the report)                  |
| CLASSIFICATION | Properties and rules by project classification                                                |
| GEO MARKER     | Geo Markers to add in the model to check georeferencing                                       |
| +\*            | All Tabs starting with "+" will be extra information and won't be taken into account by eLODy |


The details of the content of all the tabs will be given in the next paragraphs.

### Rules

The syntax for the rules are the following:

| Rule              | Description                                                           |
| ----------------- | --------------------------------------------------------------------- |
| no rule                | Checking if the property exists and has value (non blank value)       |
| =A                     | Checking if the value is equal to A                                   |
| ="OtherProperty"       | Checking if the value is equal to the value of "OtherProperty"        |
| !=A                    | Checking if the value is different from A                             |
| !="OtherProperty"      | Checking if the value is different from the value of "OtherProperty"  |
| \>10                   | Checking if the value is superior to 10                               |
| <10                    | Checking if the value is inferior to 10                               |
| A\*                    | Checking if the value starts with A                                   |
| \*A                    | Checking if the value ends with A                                     |
| \*"OtherProperty"      | Checking if the value ends with the value of "OtherProperty"          |
| \*A\*                  | Checking if the value includes A                                      |
| \[A;B;C;D;E]           | Checking if the given list contains the value                         |
| \[A*;*B]               | Checking if the value starts with A or ends with B                    |
| (+Sheet.A2:A10)        | Checking if the value is listed on Range A2 to A10 on Sheet           |
| MAPPING(+Sheet,OtherProperty)  | Checking if the value match the value on Sheet where the mapping is define by the OtherProperty value |
| CLASSIFICATION(+Sheet)         | Checking if the value match the classification specified on Sheet |
| /^a$/g            | [Regular expressions](https://regexr.com/) (here if value is a)       |

Following rules can include other property name between double quotes " ": 
- =
- !=
- \>
- <
- *A*
- [ ]

### Typical Property Checking

Include the following headers on a sheet to perform a property check

| Categories | Properties          | Rule1     | ExceptionCategories | ExceptionProperties | ExceptionRule1 |
| ---------- | ------------------- | --------- | ------------------- | ------------------- | -------------- |
| MyPset     | Building_Code       | \*09BVC\* | Element             | IFCLASS             | IfcWall        |

This example checks that every objects, except IfcWall elements, has a property "Building_Code" on a Pset called "MyPset" with a value containing "09BVC"

You can also add several rules on a property checks

| Properties          | Rule1     | Rule2 | Rule3    | Rule4  |
| ------------------- | --------- | ----- | -------- | ------ |
| Building_Code       | 09BVC\*   | \*00  |          |        |

This example checks that the property "Building_Code" starts with "09BVC" and ends with "00"

Additionally, you can define multiple exception rules

| Properties          | Rule1     | ExceptionProperties | ExceptionRule1 | ExceptionRule2 |
| ------------------- | --------- | ------------------- | -------------- | -------------- |
| Building_Code       |           | IFCLASS             | IfcWall        | IfcColumn      |

This example checks that every objects, except IfcWall AND IfcColumn elements, has a property "Building_Code"

### Reference Rule

You can reference an Excel Range has a property check rule.

| Properties           | Rule1                            |
| -------------------- |--------------------------------- |
| Building_Code        | (+Sheet.A2:A5)                   |

If the excel files contains a sheet with name "+Sheet" (start with a + sign to tell elody that this sheet does not contain property checks) with the following example:

![](vid/excel_ref.png){height=130}

is similar has the following rule 

| Properties           | Rule1                            |
| -------------------- |--------------------------------- |
| Building_Code        | \[09BVC;010TRV;011RCF;\*STA\*\]  |

Meaning that the property "Building_Code" value can be "09BVC", "010TRV", "011RCF" OR anything containg "STA"


### Mapping Rule

You can define a mapping rule with the following :

| Properties           | Rule1                            |
| -------------------- |--------------------------------- |
| Building_Code        | MAPPING(+Sheet,Source File)      |

If the excel files contains a sheet with name "+Sheet" (start with a + sign to tell elody that this sheet does not contain property checks) with the following example:

![](vid/excel_mapping.png){height=130}

This will check:

- if the value of the property "Source File" is equal to "FRP-BIM-1163-STR.ifc" then the property "Building_Code" should be equal to "STATION 1"
- if the value of the property "Source File" is equal to "FRP-BIM-1259-STR.ifc" then the property "Building_Code" should be equal to "STATION 2"
- if the value of the property "Source File" is equal to "FRP-BIM-1163-STR.ifc" then the property "Building_Code" should <u>starts with</u> to "STATION 3-"


### Classification Rule

You can define a classification rule with the following :

| Properties             | Rule1                            |
| ---------------------- |--------------------------------- |
| CLASS_Code_1           | CLASSIFICATION(+Sheet)           |
| CLASS_Code_2           | CLASSIFICATION(+Sheet)           |
| CLASS_Code_3           | CLASSIFICATION(+Sheet)           |

If the excel files contains a sheet with name "+Sheet" (start with a + sign to tell elody that this sheet does not contain property checks) with the following example:

![](vid/excel_classification.png){height=130}

This will check:

- if the value of the property "CLASS_Code_1" is equal to "Infrastructure", the property "CLASS_Code_2" is equal to "Bridges" and the property of "CLASS_Code_3" is equal to "Foundation" then all three properties are correct
- if the value of the property "CLASS_Code_1" is equal to "System", the property "CLASS_Code_2" is equal to "Bridges" and the property of "CLASS_Code_3" is equal to "Foundation" then "CLASS_Code_1" is correct but "CLASS_Code_2" and "CLASS_Code_3" are not


## Excel Tabs

### File Data Tab

Here is the details to create checking rules for file data information.

#### Filename

In order to check the filename, please follow the example below:

| Subject    | Properties | Rule1                            |
| ---------- | -----------|--------------------------------- |
| Filename   | Separator  | =\_                              |
| Filename   | ID_CLIENT1 | =PN1539                          |
| Filename   | ID_CLIENT2 | =17                              |
| Filename   | ID_CLIENT3 | =EXE                             |
| Filename   | ID_CLIENT4 | =MOD                             |
| Filename   | ID_CLIENT5 | /^\[0-9]{6}$/g                   |
| Filename   | ID_CLIENT6 | \[01;02;03;04;05;06;07;08;09;10] |
| Filename   | ID_CLIENT7 | \[01;02;03;04;05;06;07;08;09;10] |
| Filename   | ID_CLIENT8 | =09BVC                           |
| Filename   | ID_CLIENT9 | \[ACC;ACO;AMG]                   |

Subject, Properties and Rule1 have to be placed in the first line of the tab.

The separator is equal sign followed by the character to use to split the filename in different parts. ID_CLIENTX is the part to check inside the filename.

#### Levels

In order to check the filename, please follow the example below:

| Subject | Properties        | Rule1                                                        |
| ------- | ------------------|------------------------------------------------------------- |
| Levels  | ExactMatch Option | 1                                                            |
| Levels  | Level Name        | [NTT;N01;N00;S01-1;S01;S02;S03;S04;N-Ref;Zra]                |
| Levels  | Level Elevation   | [93,12;89,54;85,22;77,7;77,22;69,22;62,18;59,78;62,18;61,08] |
| Levels  | Level Property    | [eLODy Rule](#rules)                                         |

Subject, Properties and Rule1 have to be placed in the first line of the tab.

The exact match option (0 or 1) allows to check for the exact elevation for a specific name. Of course, level name and level elevations tabs must have the same length.

In order to check other properties on level, you just need to add a new row with the property name and the related rule to check.

#### Project Information

This check will only be performed if input file is RVT. In order to check the properties of Project Information, please follow the example below:

| Subject            | Properties           | Rule1                                                                 |
| ------------------ | ---------------------|---------------------------------------------------------------------- |
| ProjectInformation | Nom du marché        |
| ProjectInformation | Nom du bâtiment      |
| ProjectInformation | Spécialité           |
| ProjectInformation | Phase                | =EXE
| ProjectInformation | Ligne SGP            |
| ProjectInformation | Ouvrage SGP          | =09BVC
| ProjectInformation | Intergare SGP        | [14XIL;13XIL;12XIL;11XIL;10XIL;09XIL;08XIL]                                                 |
| ProjectInformation | Tunnel SGP           | [15T01;14T02;14T01;14TSM;13T02;13T01;12T01;11T02;11T01;10T02;10T01;09T01;08T02;08T01;08TSM] |
| ProjectInformation | Date de fin de phase |
| ProjectInformation | Nom du client        | =Société du Grand Paris                                                                     |
| ProjectInformation | Adresse du projet    |

Subject, Properties and Rule1 have to be placed in the first line of the tab.


### IFC Data Tab

Here is the details to create checking rules for IFC Class information.

#### IfcProject

This check will only be performed if input file is IFC. In order to check the properties of IfcProject, please follow the example below:

| IFCClass   | Properties | Rule1            |
| ---------- | -----------|----------------- |
| IFCPROJECT | NAME       | Autodesk Revit\* |
| IFCPROJECT | VERSION    | =2019            |

Subject Properties and Rule1 have to be placed in the first line of the tab.

#### IfcSite

This check will only be performed if input file is IFC. In order to check the properties of IfcSite, please follow the example below :

| IFCClass | Properties           | Rule1 |
| -------- | -------------------- | ----- |
| IFCSITE  | RefLatitude_Degrees  |
| IFCSITE  | RefLatitude_Minutes  |
| IFCSITE  | RefLatitude_Seconds  |
| IFCSITE  | RefLongitude_Degrees |
| IFCSITE  | RefLongitude_Minutes |
| IFCSITE  | RefLongitude_Seconds |

IFCClass, Properties and Rule1 have to be placed in the first line of the tab.

#### IfcBuilding

This check will only be performed if input file is IFC. In order to check the properties of IfcBuilding, please follow the example:

| IFCClass    | Properties           | Rule1                                                                                       |
| ----------- | ---------------------|-------------------------------------------------------------------------------------------- |
| IFCBUILDING | NAME                 | Gare de BVC                                                                                 |
| IFCBUILDING | Nom du marché        |
| IFCBUILDING | Nom du bâtiment      |
| IFCBUILDING | Spécialité           |
| IFCBUILDING | Phase                | =EXE                                                                                        |
| IFCBUILDING | Ligne SGP            |
| IFCBUILDING | Ouvrage SGP          | =09BVC                                                                                      |
| IFCBUILDING | Intergare SGP        | [14XIL;13XIL;12XIL;11XIL;10XIL;09XIL;08XIL]                                                 |
| IFCBUILDING | Tunnel SGP           | [15T01;14T02;14T01;14TSM;13T02;13T01;12T01;11T02;11T01;10T02;10T01;09T01;08T02;08T01;08TSM] |
| IFCBUILDING | Date de fin de phase |
| IFCBUILDING | Nom du client        | =Société du Grand Paris                                                                     |
| IFCBUILDING | Adresse du projet    |

IFCClass, Properties and Rule1 have to be placed in the first line of the tab.


### Generic Properties Tab

In order to check properties for all the object of a model, please follow this example:

| Properties              | Rule1                                          | Rule2 | Rule3    | Rule4  |
| ----------------------- | ---------------------------------------------- | ----- | -------- | ------ |
| SGP_DI_Sous_Ouvrage     | \*09BVC\*                                      | \*B\* | \*XXXX\* | \*00\* |
| SGP_DI_Ouvrage          | =09BVC                                         |       |          |
| SGP_DI_Batiment         | =B                                             |       |          |
| SGP_DI_Niveau           | \[NTT;N01;N00;S01-1;S01;S02;S03;S04;N-Ref;Zra] |       |          |
| SGP_DI_Typologie_Espace | \[ABVE;ACAS;ACCU;ACFA;ACHA]                    |       |          |
| SGP_DI_Incrementation   | /^\[0-9]{2}$/g                                 |       |          |

Properties and RuleX have to be placed in the first line of the tab.

### Classification

In order to check the properties of each class of project classification, please follow this example:

| Uniclass2015   | Properties          | Rule1                                       | Rule2   |
| -------------- | ------------------- | ------------------------------------------- | ------- |
| Ss_20_05_65    | ClassifName         |                                             |
| Ss_20_05_65    | WBS_Code            | HS2\*                                       | \*PIL\* |
| Ss_20_05_65    | CBS_Code            | HS2\*                                       | \*PIL\* |
| Ss_20_05_65    | Reinforcement Ratio | \*kg/m3\*                                   | >100    |
| Ss_20_05_65    | Grade_Mix           | C\*                                         |
| Ss_20_05_65    | Volume              | <100                                        |
| Ss_20_05_65    | Length              | <100                                        |
| Ss_20_05_65    | Section Name        |                                             |
| Ss_20_05_65    | Layer               | =CB-Ss_20_05_65-M_PilesConcreteInsitu       |
|                |                     |
| Ss_20_05       | ClassifName         |                                             |
| Ss_20_05       | WBS_Code            | HS2\*                                       | \*GSL\* |
| Ss_20_05       | CBS_Code            | HS2\*                                       | \*GSL\* |
| Ss_20_05       | Layer               | =CB-Ss_20_05-M_Blinding                     |
| Ss_20_05       | Reinforcement Ratio | \*kg/m3\*                                   |
| Ss_20_05       | Grade_Mix           | C\*                                         |
| Ss_20_05       | Volume              |                                             |
| Ss_20_05       | Section Name        |                                             |
| Ss_20_05       | Material            |                                             |
|                |                     |
| Ss_20_05_15_71 | ClassifName         |                                             |
| Ss_20_05_15_71 | WBS_Code            | HS2\*                                       | \*PLC\* |
| Ss_20_05_15_71 | CBS_Code            | HS2\*                                       | \*PLC\* |
| Ss_20_05_15_71 | Layer               | =CB-Ss_20_05_15_71-M_PilecapsConcreteInsitu |
| Ss_20_05_15_71 | Reinforcement Ratio | \*kg/m3\*                                   |
| Ss_20_05_15_71 | Grade_Mix           | C\*                                         |
| Ss_20_05_15_71 | Volume              |                                             |
| Ss_20_05_15_71 | Thickness           |                                             |

Properties and RuleX have to be placed in the first line of the tab.

The classification parameter name have to be in first line and first column.

Possible Classification Values have to be set with ClassifName in Properties.

Property category checking can be added (see [Property Category Checking](#property-category-checking)).

### Ignore Objects

In order to ignore objects from the check, you just need to define the property and the associated rules in a new tab named IGNORE. If the properties are found in the object and the rules are validated the objects will be ignored from the check.

| Properties        | Rule1                                                     |
| ----------------- | --------------------------------------------------------- |
| Nom de la famille | =L15T2_MRO_GEN_Sphere-Niveau;L15T2_MRO_GEN_BorneRepere-01 |

### Geo Markers

In order to check automatically the georeferencing, you will need to define the following parameters in the Excel file. This process can be used when BIM Manager has requested to place a specific object (called geomarker) in all the BIM models of the project. eLODy will thus search for this object in the models thanks to its name prodided in the configuration. The DELTA value is the acceptable offset in 3D of the model gormarker from the X,Y,Z provided in the configuration.

| FILE             | X           | Y           | Z     | NAME                         | DELTA |
| ---------------- | ----------- | ----------- | ----- | ---------------------------- | ----- |
| file1.rvt        | 1665232.471 | 8180535.021 | 62.18 | L15T2_MRO_GEN_BorneRepere-01 | 0.01  |
| \*-CODE-\*.rvt   | 1665173.842 | 8180567.483 | 65.49 | \*_BorneRepere\*             | 0.01  |

FILE and NAME can includes strict equality (with or without equal sign), different than (!=) or wildchar for starts with, ends with or includes.

## Example

Example file and parameter can be found in this folder: [Examples](https://gitlab.com/systra/dev4bim/elody/documentation/-/tree/main/examples)