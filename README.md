# ELODY

## Summary

- [Introduction](#introduction)
- [Usage](#usage)
   - [With Acc](#with-acc)
      - [Folder and file navigation](#folder-and-file-navigation)
      - [Import a data schema](#import-a-data-schema)
      - [Access project data schemas](#access-project-data-schemas)
   - [Per user](#per-user)
   - [Viewer](#viewer)
      - [Check a model](#check-a-model)
      - [Analyze checks result](#analyze-checks-result)
      - [Download Check Report](#download-check-report)
- [Data Schema](#data-schema)
- [Technologies](#technologies)

## Introduction

ELODY is a web application to control the data in BIM (Building Information Modeling) Models. The control is based on a data schema given by the user and exports can be done to the following formats: Excel and [BCF](https://en.wikipedia.org/wiki/BIM_Collaboration_Format).

**Where we are:**

- In most of our BIM Models delivery, focus is made on design/modeling, but BIM Models additional value is about data
- No tool/process dedicated to the quality of data for BIM Models (Manual Checking/Massive Export to Excel/ad hoc development on modeling software)
- Designers are responsible of the data provided in BIM objects but in most of the case they are not the ones in charge of implementing the data and the checking too…
- Ensure the compliance of Data Schema and Objects Classifications defined in the AIR/EIR (Asset Information Requirements/Employer Information Requirements) or BEP (BIM Execution Plan)

**What we need:**

- Provide to designers an easy-to-use tool to check data quality of BIM Models : different from a production/modeling tool, web-application => accessible through an internet browser
- Multiple file format compatible : IFC, RVT, DWG, DGN, NWD…
- Help client/designer to control the quality of BIM deliverables to enhance the use of BIM Models (Cost Estimation, BOQ, Opex, etc)

## Usage

To access the tool follow this link : [https://elody.systra.digital](https://elody.systra.digital) and connect using your Azure Account.

eLODy work with two differents modes :
1. With Autodesk Construction Cloud Project
2. Standalone, single user and file based

### With ACC
Once you have accessed eLODy you can logged in your Autodesk Construction Cloud, access to your projects and files :

![](vid/connection_acc.webm){width=600}


#### Folder and file navigation

![](vid/navigation.png){width=600}

The action menu depends on the file type (excel, 3D models and others). Every file type has the "Open" menu that will open the file in ACC in a new tab.

#### Import a data schema

In order to check a model, you first need an excel data schema containing the set of rules to check against your model. (To access documentation about data schema click [here](EXCEL.md)). You can store this excel file anywhere in your ACC project folders.

Once your data schema file is ready make it available by eLODy :

Open the actions menu and select "Import" 

![](vid/import_data_schema.webm){width=600}

If a new version of the file is created in ACC this version will be automatically imported in eLODy. You can import as many data schema as you want within a project.

Action Menu :

- Open : to open the file in ACC in a new tab
- Import : to register the excel file as a data schema for the project

#### Open or check a model

A 3D model stored in ACC can be open by clicking on it, or by open the action menu and by selecting "Open in 3D". This will open the Autodesk Platform Service Viewer, to access documentation about checking a model with the viewer click [here](#technologies)

Action Menu :

- Open : to open the file in ACC in a new tab
- Open in 3d : to open the file in the Autodesk Platform Service Viewer with eLODy
- Add : to add this file to the list of model available in batch mode
- Check : to perform a check of this file with the active data schema

#### Access project data schemas

To access the project imported data schema you can select "Elody configuration files for project" button above the folder navigation.

![](vid/project_data_schema.png){width=600}

On the page you can set a data schema as active, meaning this data schema will be used when selecting "Check" on a model action menu and also delete the data schema (this will not delete the file on ACC but unregister the data schema within eLODy)

### Per User

You can also upload files manually from your computer to eLODy.

![](vid/upload_file.webm){width=600}

> :warning:
**If your internet connection is slow, the file may not properly be uploaded.[- Fiber optic internet is strongly recommanded and is mandatory for files above 200Mo -]**

### Viewer

![](vid/viewer.png){width=600}

#### Check a model

To check a model click on "Launch eLODy Configuration", and select your excel data schema file (either from ACC imported schema or from you computer).

![](vid/check_model.webm){width=600}

Once the server has finish checking the model, the panel will automatically closed and colored are applied to the objects.

![](vid/legend.png){height=130}

- Ignored objects are objects matching a check in the [ignore tab](EXCEL.md#ignore-objects) of the data schema.
- Not classfied objects are objects not matching a classification value or not missing the classification parameter check in the [classification tab](EXCEL.md#classification) of the data schema.
- Not validated objects are objects not matching a check in either the [classification tab](EXCEL.md#ignore-objects) or the [generic tab](EXCEL.md#generic-properties-tab) of the data schema.
- Validated objects are objects matching every checks in both the [classification tab](EXCEL.md#ignore-objects) or the [generic tab](EXCEL.md#generic-properties-tab) of the data schema.

#### Analyze checks result

You can access the analytics result of the checks, File information and IFC structure checks result from the "eLODy Data Analytics" button

![](vid/analytics.png){width=600}

You can cluster objects based on any properties values from the "Clustering" button

![](vid/cluster.png){width=600}

You can also color objects based on any properties values

![](vid/color_property.png){width=600}

You can access the list of every checks made on every objects properties from the "eLODy Objects Errors"

![](vid/objects_errors.png){width=600}

You can access the list of every checks made on a single object from the "eLODy Object Error". Select an object then click on "Get from Selection" and "Apply"

![](vid/object_details.png){width=600}

#### Download Check Report

Excel and BCF reports can be produced for a model checks.

![](vid/export_options.PNG){width=600}

##### Export Options

The first option "Export Format" : Excel or BCF.

From this point every other options only affects the last excel sheet "Full Report", or the content of the BCF file

The second option "Error Categories" allow you to include in the Full Report sheet the list of error for:
- File Data : file naming convention, levels and IFC checks (not included in BCF)
- Georeferencing (not included in BCF)
- Objects : generic and classification checks on objects

The third option "Error Status" allow you to include, in the Full Report sheet or the BCF file, the list of error for objects based on their check result status

The fouth option "Export other parameter in Report" allow you to include in the Full report sheet some additional properties values for the objects.
> :warning:
**Requesting additional properties could seriously increase the time needed for the report to be computed.**

Then you have three more options:
- "Group Issues by object" : if the option is selected, an object with several properties not matching requirements will be listed on a single excel row. if not a row will be added for each properties
- "Group Issues by level" : same as "Group Issues by object" for level errors
- "Include Object Screenshots" : only available for BCF export, this will add a screenshot of every objects.
> :warning:
**Requesting object screenshots could seriously increase the time needed for the report to be computed.**


## Data Schema

To access documentation [a relative link](EXCEL.md)


## Technologies

eLODy is a web application based on the following technologies:

- Frontend: [React](https://fr.reactjs.org/) [Typescript](https://www.typescriptlang.org/)
- Backend: [Node JS](https://nodejs.org/en/) [Typescript](https://www.typescriptlang.org/)

Both Backend and FrontEnd used [Autodesk Forge](https://forge.autodesk.com/) APIs and Viewer.
